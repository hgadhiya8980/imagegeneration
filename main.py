from flask import (Flask, send_from_directory, request, session)
from flask_cors import CORS
import re
from dotenv import load_dotenv
import google.generativeai as genai
from datetime import datetime
from IPython.display import Image as IPImage
import os
import json
import logging
import requests
import textwrap
from pymongo import MongoClient
from PIL import Image, ImageDraw, ImageFont, ImageFilter

app = Flask(__name__)

CORS(app)

app.config["enviroment"] = "qa"
app.config["SECRET_KEY"] = "sdfsf65416534sdfsdf4653"

app.config['IMAGE_FOLDER'] = 'output_image/'

# handling our application secure type like http or https
secure_type = "http"

load_dotenv()

logging.basicConfig(filename="server.log", level=logging.DEBUG)
client = MongoClient("mongodb+srv://harshitgadhiya8980:harshitgadhiya8980@cluster0.xradpzd.mongodb.net/")

GOOGLE_API_KEY = os.getenv("GOOGLE_API_KEY")
genai.configure(api_key=GOOGLE_API_KEY)

model = genai.GenerativeModel('models/gemini-1.0-pro-vision-latest')

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif' 'svg'}


def allowed_photos(filename):
    """
    checking file extension is correct or not

    :param filename: file name
    :return: True, False
    """
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def text_extract(detail, wrap_width):
    try:
        wrapped_text = textwrap.fill(detail, wrap_width)
        return wrapped_text

    except Exception as e:
        print(e)

def download_image(image_url, save_path):
    try:
        response = requests.get(image_url)
        if response.status_code == 200:
            with open(save_path, 'wb') as f:
                f.write(response.content)
        else:
            print("Error: Unable to download image")
    except Exception as e:
        print("Error:", e)

def get_gemini_text_response(prompt):
    try:
        model = genai.GenerativeModel('models/gemini-1.0-pro-latest')
        response = model.generate_content(prompt)
        response.resolve()
        response_text = response.text
        return response_text

    except Exception as e:
        print(f"Error in get_gemini_text_response: {e}")

def get_response_format(niche, count):
    try:
        count = str(count)
        prompt = 'can you please give me' + count + 'short title and short motivational text with given niche only 1 sentense for given niche="' + niche + '". give me response format like [{"title": "cricket", "content_text": "play cricket with high potencial"}, {"title": "", "content_text": ""}]'
        response_text = get_gemini_text_response(prompt)
        output_li = json.loads(response_text)
        return output_li
    except Exception as e:
        print(e)

def get_image_path(title):
    try:
        url = f"https://api.pexels.com/v1/search?query={title}"

        payload = {}
        headers = {
            'Authorization': 'dkWMfoYyGjY1RlioSjUMJLd9ar3a2M5j4rFhNsCBrC8zlNLkF2qOmFTA',
            'Cookie': '__cf_bm=uFvqUR5AVEvYmn7RkCY6X08CwrjNxFpOgyUI0F.PG_0-1711619162-1.0.1.1-c2Cdv.swHMpbkuNT7VUKZhmOC8g_nIIrwmDmBfuKcziVvJnQC0Tw2XHREY7uYPx6efZacfzttTq7euPjkyr6vQ; __cf_bm=G77RA5CmIQ3sYgmE7FtDvIM.yC6yH7JY5c4vz3h2BZY-1711623518-1.0.1.1-qjFqI6WuxgHQq5joFdELZ6MYR.d9zzSWlJ9tdQGGVJIuUSKu.45A_L2KLrYG9EwOK.d3J19omFwkQvclwW0vbA'
        }

        response = requests.request("GET", url, headers=headers, data=payload)

        response_data = json.loads(response.text)
        imageurl = response_data["photos"][0]["src"]["portrait"]
        filename = imageurl.split("?")[0].split("/")[-1]
        download_image(imageurl, "backgrounds/"+filename)
        filepath = "backgrounds/"+filename
        return filepath, filename

    except Exception as e:
        print(e)

@app.route('/download/<filename>')
def download_image_path(filename):
    return send_from_directory(app.config['IMAGE_FOLDER'], filename, as_attachment=True)

@app.route('/view/<filename>')
def view_image_path(filename):
    return send_from_directory(app.config['IMAGE_FOLDER'], filename)

def generate_image(title, text_content, filepath, filename):
    try:
        background_image = Image.open(filepath).convert("RGB")

        # Create black overlay with opacity
        overlay_color = (0, 0, 0, 128)  # Black with 50% opacity (adjust alpha for desired transparency)
        overlay_image = Image.new("RGBA", background_image.size, overlay_color)

        # Paste the overlay onto the background image
        background_image.paste(overlay_image, mask=overlay_image)

        textextract = text_extract(text_content, 20)
        textextract = textextract.split("\n")

        titleextract = text_extract(title, 10)
        titleextract = titleextract.split("\n")

        startvalue = 300
        text_data = []
        if len(titleextract)!=0:
            for varnew in titleextract[:-1]:
                text_data.append((varnew, "abold.ttf", 70, (255, 255, 255), (120, startvalue)))
                startvalue+=100
            text_data.append((titleextract[-1], "abold.ttf", 70, (255, 255, 255), (120, startvalue)))
            startvalue += 120
        else:
            text_data.append((title, "abold.ttf", 70, (255, 255, 255), (120, startvalue)))
            startvalue += 120

        add_count = 80
        for var in textextract:
            text_data.append((var, "body.ttf", 40, (255, 255, 255), (120, startvalue)))
            startvalue = startvalue+add_count

        # Create a drawing object to add text
        draw = ImageDraw.Draw(background_image)

        # Add each text element with its specific style
        for text, font_path, font_size, color, position in text_data:
            font = ImageFont.truetype(font_path, font_size)
            draw.text(position, text, fill=color, font=font)

        # Save the modified image
        new_image_path = app.config['IMAGE_FOLDER']+filename
        background_image.save(new_image_path, format="PNG")

        return "true", filename

    except Exception as e:
        print(e)
        return "false", ""

def get_response(filename, filetextname):
    """_summary_
    find output like color,  category and pattern for given image
    Args:
        image_path (_type_): relative path of image
    """
    try:
        # Save the PIL image to a local file
        pil_image = Image.open(filename)
        pil_image.save(filename)

        # Create IPython Image object
        img = IPImage(filename=filename)

        response = model.generate_content(["providing the output in the same language. Extract the following entities and keys like [person_names, designations, contact_numbers, website_urls, locations, services, email_addresses, and company_names from the given content text='{all_content}'  Provide the output in JSON format only. No additional information is required, only the JSON output.", img])
        response.resolve()
        response_text = response.text
        app.logger.debug(f"response text from image: {response_text}")
        try:
            json_data = re.search(r'\{.*\}', response_text, re.DOTALL).group(0)
            output = json.loads(json_data)
            output["filename"] = filetextname
        except Exception as e:
            pass
        app.config["all_output"].append(output)

    except Exception as e:
        print(f"Error in get response in filename: {e}")

def get_timestamp(app):
    try:
        current_datetime = datetime.now()
        formatted_datetime = current_datetime.strftime("%m-%d-%Y %H:%M:%S")

        return formatted_datetime

    except Exception as e:
        app.logger.debug(f"Error in get timestamp: {e}")

def get_response_msg(app, status, statuscode, message, data):
    try:
        response_data_msg = {
            "timestamp": get_timestamp(app),
            "status": status,
            "statusCode": statuscode,
            "message": message,
            "data": data
        }

        return response_data_msg

    except Exception as e:
        app.logger.debug(f"Error in get response msg: {e}")

def get_error_msg(app, e):
    try:
        response_data_msg = {
          "data": "null",
          "message": f"Server Not Responding Error {e}",
          "status": "FORBIDDEN",
          "statusCode": 403,
          "timestamp": get_timestamp(app)
        }

        return response_data_msg

    except Exception as e:
        app.logger.debug(f"Error in get response msg: {e}")

@app.route("/image_generation/v1/get_images", methods=["GET", "POST"])
def get_images():
    """
    In this route we can handling student register process
    :return: register template
    """
    try:
        if request.method == "POST":
            app.logger.debug("coming in register api")
            title = request.form.get("title", "nothing")
            user_text = request.form.get("user_text", "nothing")
            output_data = []

            mapping_dict = {}
            filepath, filename = get_image_path(title)
            output_res, image_name = generate_image(title, user_text, filepath, filename)
            if output_res == "true":
                mapping_dict["view_image"] = f"http://52.66.149.133:4142/view/{image_name}"
                mapping_dict["download_image"] = f"http://52.66.149.133:4142/download/{image_name}"
                output_data.append(mapping_dict)
            else:
                print("image not generate")

            response_data = get_response_msg(app, "SUCCESS", 200, "Image Stored", {"data": output_data})
            return response_data
        else:
            response_data = get_response_msg(app, "FAILURE", 500, "Please use post method", {})
            return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        app.logger.debug(f"Error in add student data route: {e}")
        return response_data

@app.route("/image_generation/v1/get_content", methods=["GET", "POST"])
def get_content():
    """
    In this route we can handling student register process
    :return: register template
    """
    try:
        if request.method == "POST":
            app.logger.debug("coming in register api")
            niche = request.form.get("text", "nothing")
            count = request.form.get("count", "nothing")
            output_data = []

            count = int(count)
            output_li = get_response_format(niche, count)

            response_data = get_response_msg(app, "SUCCESS", 200, "Content generated", {"Contents": output_li})
            return response_data
        else:
            response_data = get_response_msg(app, "FAILURE", 500, "Please use post method", {})
            return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        app.logger.debug(f"Error in get content route: {e}")
        return response_data


if __name__ == '__main__':
    app.run(port=4142, host="0.0.0.0")