from dotenv import load_dotenv
import google.generativeai as genai
from PIL import Image
from IPython.display import Image as IPImage
import os
import json
import requests
import textwrap
from PIL import Image, ImageDraw, ImageFont, ImageFilter

load_dotenv()

GOOGLE_API_KEY = os.getenv("GOOGLE_API_KEY")
genai.configure(api_key=GOOGLE_API_KEY)

def text_extract(detail, wrap_width):
    try:
        wrapped_text = textwrap.fill(detail, wrap_width)
        return wrapped_text

    except Exception as e:
        print(e)

def download_image(image_url, save_path):
    try:
        response = requests.get(image_url)
        if response.status_code == 200:
            with open(save_path, 'wb') as f:
                f.write(response.content)
        else:
            print("Error: Unable to download image")
    except Exception as e:
        print("Error:", e)

def get_gemini_text_response(prompt):
    try:
        model = genai.GenerativeModel('models/gemini-1.0-pro-latest')
        response = model.generate_content(prompt)
        response.resolve()
        response_text = response.text
        return response_text

    except Exception as e:
        print(f"Error in get_gemini_text_response: {e}")

def get_response_format(niche, count):
    try:
        count = str(count)
        prompt = 'can you please give me' + count + 'short title and short motivational text with given niche only 1 sentense for given niche="' + niche + '". give me response format like [{"title": "cricket", "content_text": "play cricket with high potencial"}, {"title": "", "content_text": ""}]'
        response_text = get_gemini_text_response(prompt)
        output_li = json.loads(response_text)
        return output_li
    except Exception as e:
        print(e)

def get_image_path(title):
    try:
        url = f"https://api.pexels.com/v1/search?query={title}"

        payload = {}
        headers = {
            'Authorization': 'dkWMfoYyGjY1RlioSjUMJLd9ar3a2M5j4rFhNsCBrC8zlNLkF2qOmFTA',
            'Cookie': '__cf_bm=uFvqUR5AVEvYmn7RkCY6X08CwrjNxFpOgyUI0F.PG_0-1711619162-1.0.1.1-c2Cdv.swHMpbkuNT7VUKZhmOC8g_nIIrwmDmBfuKcziVvJnQC0Tw2XHREY7uYPx6efZacfzttTq7euPjkyr6vQ; __cf_bm=G77RA5CmIQ3sYgmE7FtDvIM.yC6yH7JY5c4vz3h2BZY-1711623518-1.0.1.1-qjFqI6WuxgHQq5joFdELZ6MYR.d9zzSWlJ9tdQGGVJIuUSKu.45A_L2KLrYG9EwOK.d3J19omFwkQvclwW0vbA'
        }

        response = requests.request("GET", url, headers=headers, data=payload)

        response_data = json.loads(response.text)
        imageurl = response_data["photos"][0]["src"]["portrait"]
        filename = imageurl.split("?")[0].split("/")[-1]
        download_image(imageurl, "backgrounds/"+filename)
        filepath = "backgrounds/"+filename
        return filepath, filename

    except Exception as e:
        print(e)

def generate_image(title, text_content, filepath, filename):
    try:
        background_image = Image.open(filepath).convert("RGB")

        # Create black overlay with opacity
        overlay_color = (0, 0, 0, 128)  # Black with 50% opacity (adjust alpha for desired transparency)
        overlay_image = Image.new("RGBA", background_image.size, overlay_color)

        # Paste the overlay onto the background image
        background_image.paste(overlay_image, mask=overlay_image)

        textextract = text_extract(text_content, 20)
        textextract = textextract.split("\n")

        titleextract = text_extract(title, 10)
        titleextract = titleextract.split("\n")

        startvalue = 300
        text_data = []
        if len(titleextract)!=0:
            for varnew in titleextract[:-1]:
                text_data.append((varnew, "abold.ttf", 70, (255, 255, 255), (120, startvalue)))
                startvalue+=100
            text_data.append((titleextract[-1], "abold.ttf", 70, (255, 255, 255), (120, startvalue)))
            startvalue += 120
        else:
            text_data.append((title, "abold.ttf", 70, (255, 255, 255), (120, startvalue)))
            startvalue += 120

        add_count = 80
        for var in textextract:
            text_data.append((var, "body.ttf", 40, (255, 255, 255), (120, startvalue)))
            startvalue = startvalue+add_count

        # Create a drawing object to add text
        draw = ImageDraw.Draw(background_image)

        # Add each text element with its specific style
        for text, font_path, font_size, color, position in text_data:
            font = ImageFont.truetype(font_path, font_size)
            draw.text(position, text, fill=color, font=font)

        # Save the modified image
        new_image_path = "output_image/"+filename
        background_image.save(new_image_path, format="PNG")

        return "true"

    except Exception as e:
        print(e)
        return "false"


niche = "yoga"
count = 5
output_li = get_response_format(niche, count)

output_data = []
for var in output_li:
    title = var["title"]
    textcontent = var["content_text"]
    filepath, filename = get_image_path(title)

    output_res, image_path = generate_image(title, textcontent, filepath, filename)
    if output_res=="true":
        output_data.append(image_path)
    else:
        print("image not generate")





